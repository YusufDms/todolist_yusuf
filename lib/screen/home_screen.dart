import 'package:flutter/material.dart';
import 'package:project/screen/new_sreen.dart';

class HomeSreen extends StatefulWidget {
  const HomeSreen({Key? key}) : super(key: key);

  @override
  _HomeSreenState createState() => _HomeSreenState();
}

class _HomeSreenState extends State<HomeSreen> {
  @override
  late List<String> _todoList = <String>[];

  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
       title: const Text('Things to Do'),
        actions: [
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){
              print("clicked search");
            },
          ),
      PopupMenuButton(
        onSelected: (item) => onSelected(context, item),
        itemBuilder: (context) => [
          const PopupMenuItem <int>(
              value : 0,
              child: Text('Status')
          ),
          const PopupMenuItem <int>(
              value : 1,
              child: Text('Date')
          ),
          const PopupMenuItem <int>(
              value : 2,
              child: Text('Priority'),
          ),
          // const CheckedPopupMenuItem(
          //   value: 3,
          //   child: Text('mantap'),
          // )
        ]
      )
    ],
      ),
      body: ListView(children: _getItems()),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Navigator.push(context, MaterialPageRoute(builder: (context) => NewScreen()));
          navigateToForm();
        },
        child: Icon(Icons.add),
      ),

    );

  Widget _buildTodoItem(String title) {
    return ListTile(
      title: Text(title),
    );
  }

  List<Widget> _getItems() {
    final List<Widget> _todoWidgets = <Widget>[];
    for (String title in _todoList) {
      _todoWidgets.add(_buildTodoItem(title));
    }
    return _todoWidgets;
  }

  Future<void> navigateToForm() async {
    final result = await Navigator.push(context, MaterialPageRoute(builder: (context) => NewScreen(this._todoList)));

    setState(() {
      _todoList = result;
    });
  }
}

  void onSelected (BuildContext context, var item){
    switch (item){
      case 0:
        print('clicked Status');
        break;

      case 1:
        print('clicked Date');
        break;

      case 2:
        print('clicked Priority');
        break;

      // case 3:
      //   print('clicked mantap');
      //   break;
    }
  }



// body: SafeArea(
//   child: Container(
//     width: double.infinity,
//     padding: const EdgeInsets.symmetric(
//       horizontal: 24.0,
//       vertical: 32.0,
//     ),
//     decoration: BoxDecoration(
//       color: Colors.white,
//       borderRadius: BorderRadius.circular(20.0),
//     ),
//     child: Stack(
//       children: [
//         Column(
//           children: const [
//             Text("getstarted"),
//           ],
//         ),
//         Positioned(
//           bottom: 0.0,
//             right: 0.0,
//             child: Container(
//               width: 60.0,
//               height: 60.0,
//               decoration: BoxDecoration(
//                 color: Colors.deepOrangeAccent,
//                 borderRadius: BorderRadius.circular(50.0),
//               ),
//               child: Image(
//               //   onTap:(){
//               //     Navigator.push(context, MaterialPageRoute(builder: (context) => NewScreen()));
//               // },
//                 image: AssetImage(
//                   "assets/add_icon.png",
//                 ),
//               ),
//             )
//         )
//       ],
//     ),
//
//   ),
// ),