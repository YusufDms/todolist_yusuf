

import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:project/screen/home_screen.dart';

class NewScreen extends StatefulWidget {
  // const NewScreen({Key? key}) : super(key: key);
  List<String> todoList;
  NewScreen(this.todoList);

  @override
  _NewScreenState createState() => _NewScreenState();
}

class _NewScreenState extends State<NewScreen> {
  var dateTime;
  var timeStamp;
  String pickedDate = 'Pick the date';
  String pickedTime = 'Pick the Time';

  final _formKey = GlobalKey<FormState>();
  final TextEditingController _editTextTitle = TextEditingController();
  final TextEditingController _editTextDesc = TextEditingController();
  final TextEditingController _editTextDate = TextEditingController();
  final TextEditingController _editTextTime = TextEditingController();
  bool isSwitched = false;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dateTime = DateTime.now();

  }

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      title: Text('New'),
      leading: RaisedButton(
        onPressed: () => Navigator.pop(context),
        child: Icon(Icons.arrow_back, color: Colors.white),
        color: Colors.blue,
      )

    ),
    body: Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Form(child: Column(
          children:<Widget> [
            new TextFormField(
              decoration: InputDecoration(
                hintText: "Title",
                filled: true,
              ),
              keyboardType: TextInputType.text,
              controller: _editTextTitle,
            ),



            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                OutlinedButton(
                    onPressed: () {
                      DatePicker.showDatePicker(context,
                          showTitleActions: true,
                          minTime: DateTime(2000, 3, 5),
                          maxTime: DateTime(2030, 6, 7), onChanged: (date) {
                            print('change $date');
                          }, onConfirm: (date) {
                            setState(() {
                              String temp;
                              temp = date.toString();
                              pickedDate = temp.substring(0, 10);
                            });
                            print('confirm $date');
                            controller: _editTextDate;
                          }, currentTime: DateTime.now(), locale: LocaleType.id);
                    },
                    style: OutlinedButton.styleFrom(
                    side: BorderSide(width: 1.0, color: Colors.grey),
                      ),
                    child: Text(
                      'Date : $pickedDate',
                      style: TextStyle(color: Colors.black),
                    )),

                OutlinedButton(
                    onPressed: () {
                      DatePicker.showTimePicker(context,
                          showTitleActions: true,
                          onChanged: (time) {
                            print('change $time');
                          }, onConfirm: (time) {
                            setState(() {
                              String temp;
                              temp = time.toString();
                              pickedTime = temp.substring(11, 19);
                            });
                            print('confirm $time');
                            controller: _editTextTime;
                          }, currentTime: DateTime.now(), locale: LocaleType.id);
                    },
                    style: OutlinedButton.styleFrom(
                      side: BorderSide(width: 1.0, color: Colors.grey),
                    ),
                    child: Text(
                      'Time : $pickedTime',
                      style: TextStyle(color: Colors.black),
                    )),
              ],
            ),
            TextFormField(
              decoration: InputDecoration(
                hintText: "Desctiprion",
                filled: true,
              ),
              keyboardType: TextInputType.text,
              controller: _editTextDesc,
            ),




          ],
        ),
        )
      ],
    ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              _addTodoItem(_editTextTitle.text);
              _addTodoItemDesc(_editTextDesc.text);
              _addTodoItemDate(_editTextDate.text);
              _addTodoItemTime(_editTextTime.text);

              Navigator.pop(context, widget.todoList);
              // Navigator.pop(context);
            },
            child: Icon(Icons.check),
    ),
  );

  void _addTodoItem(String title) {
    setState(() {
      widget.todoList.add(title);
    });
    _editTextTitle.clear();
  }

  void _addTodoItemDesc(String desc) {
    setState(() {
      widget.todoList.add(desc);
    });
    _editTextDesc.clear();
  }

  void _addTodoItemDate(String date) {
    setState(() {
      widget.todoList.add(date);
    });
    _editTextDate.clear();
  }

  void _addTodoItemTime(String time) {
    setState(() {
      widget.todoList.add(time);
    });
    _editTextTime.clear();
  }
}
