// final String TabTask ='task';
// class Field{
//   static final List<String> values = [
//     id,
//     title,
//     date,
//     time,
//     desc,
//   ];
//
//   static final String id = '_id';
//   static final String title = '_title';
//   static final String date = '_date';
//   static final String time = '_time';
//   static final String desc = '_desc';
// }
//
// class Task {
//   static int number = 0;
//   var id;
//   var title;
//   var date;
//   var time;
//   var desc;
//
//   Task({
//    this.id,
//    required this.title,
//    required this.date,
//    required this.time,
//    required this.desc,
// });
//
//   Task copy({
//   int? id,
//   String? title,
//     String? date,
//     String? time,
//     String? desc, }) => Task(
//     id: id ?? this.id,
//     title: title ?? this.title,
//     date: date ?? this.date,
//     time: time ?? this.time,
//     desc: desc ?? this.desc
//   );
//
//   Task.empty(){
//     this.title = '';
//     this.date = '';
//     this.time = '';
//     this.desc = '';
//   }
//
//   Map<String, Object?> toJson() => {
//     Field.id : this.id,
//     Field.title : this.title,
//     Field.date : this.date,
//     Field.time : this.time,
//     Field.desc : this.desc,
//   };
//
//   static Task fromjJson (Map<String, Object?> json){
//     return Task(
//       id:json[Field.id] as int?,
//         title: json[Field.title] as int?,
//         date: json[Field.date] as int?,
//         time: json[Field.time] as int?,
//         desc: json[Field.desc] as int?
//     );
//   }
//
//   Map <String ,dynamic> toMap() {
//     return{
//       'id' : id,
//       'title' : title,
//       'date' : date,
//       'time' : time,
//       'desc' : desc,
//     };
//   }
//
//   //setter
// set setTitle (String title) {
//     this.title = title;
// }
//   set setDate (String date ){
//     this.date = date;
//   }
//   set setTime (String time) {
//     this.time = time;
//   }
//   set setDesc (String desc) {
//     this.desc = desc;
//   }
//
//
//   @override
//   String toString() {
//     return 'ToDo{id: $id, title: $title, desc: $desc}';
//   }
// }